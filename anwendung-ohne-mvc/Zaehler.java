import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Zaehler {

    public void run() {
        JFrame fenster = new JFrame();
        fenster.setSize(400, 400);
        fenster.setLayout(null);

        JButton buttonErhoehen = new JButton("Erhöhen");
        buttonErhoehen.setBounds(100, 100, 200, 50);

        JButton buttonVeringern = new JButton("Veringern");
        buttonVeringern.setBounds(100, 170, 200, 50);

        JPanel panelSchrittgroesse = new JPanel();
        panelSchrittgroesse.setBounds(100, 230, 200, 80);

        JRadioButton radioButtonEinerSchritte = new JRadioButton("Einerschritte");
        radioButtonEinerSchritte.setSelected(true);
        panelSchrittgroesse.add(radioButtonEinerSchritte);

        JRadioButton radioButtonZehnerSchritte = new JRadioButton("Zehnerschritte");
        panelSchrittgroesse.add(radioButtonZehnerSchritte);

        ButtonGroup radioButtonGroupSchrittgroesse = new ButtonGroup();
        radioButtonGroupSchrittgroesse.add(radioButtonEinerSchritte);
        radioButtonGroupSchrittgroesse.add(radioButtonZehnerSchritte);

        fenster.add(panelSchrittgroesse);

        JLabel labelZaehlerWert = new JLabel(String.valueOf(0));
        labelZaehlerWert.setHorizontalAlignment(SwingConstants.CENTER);
        labelZaehlerWert.setBounds(150, 50, 100, 50);
        fenster.add(labelZaehlerWert);

        buttonErhoehen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int aktuellerZaehlerWert = Integer.parseInt(labelZaehlerWert.getText());
                int schrittgroesse;
                if (radioButtonEinerSchritte.isSelected()) {
                    schrittgroesse = 1;
                } else if (radioButtonZehnerSchritte.isSelected()) {
                    schrittgroesse = 10;
                } else {
                    throw new RuntimeException("Keine gültige Schrittgröße ausgewählt!");
                }
                int neuerZaehlerWert = aktuellerZaehlerWert + schrittgroesse;
                labelZaehlerWert.setText(String.valueOf(neuerZaehlerWert));
            }
        });

        fenster.add(buttonErhoehen);

        buttonVeringern.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int aktuellerZaehlerWert = Integer.parseInt(labelZaehlerWert.getText());
                int schrittgroesse;
                if (radioButtonEinerSchritte.isSelected()) {
                    schrittgroesse = 1;
                } else if (radioButtonZehnerSchritte.isSelected()) {
                    schrittgroesse = 10;
                } else {
                    throw new RuntimeException("Keine gültige Schrittgröße ausgewählt!");
                }
                int neuerZaehlerWert = aktuellerZaehlerWert - schrittgroesse;
                labelZaehlerWert.setText(String.valueOf(neuerZaehlerWert));
            }
        });

        fenster.add(buttonVeringern);

        fenster.setVisible(true);
    }

    public static void main(String[] args) {
        Zaehler meinZaehler = new Zaehler();
        meinZaehler.run();
    }

}
