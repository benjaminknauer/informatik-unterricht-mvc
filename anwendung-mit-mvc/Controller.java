import java.awt.event.ActionEvent;

public class Controller {

    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        this.view.addButtonErhoehenActionListener(this::erhoeheZaehler);
        this.view.addButtonVeringernActionListener(this::veringereZaehler);
        this.view.addRadioButtonEinerSchritteActionListener(this::aktualisiereSchrittgroesse);
        this.view.addRadioButtonZehnerSchritteActionListener(this::aktualisiereSchrittgroesse);

        this.view.show();
    }

    private void erhoeheZaehler(ActionEvent e) {
        this.model.setZahlerWert(model.getZahlerWert() + model.getSchrittgroesse());
        this.view.setzeZaehlerWert(this.model.getZahlerWert());
    }

    private void veringereZaehler(ActionEvent e) {
        this.model.setZahlerWert(model.getZahlerWert() - model.getSchrittgroesse());
        this.view.setzeZaehlerWert(this.model.getZahlerWert());
    }

    private void aktualisiereSchrittgroesse(ActionEvent e) {
        this.model.setSchrittgroesse(this.view.gibAusgewaehlteSchrittgroesse());
    }

}
