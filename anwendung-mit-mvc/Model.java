public class Model {

    private int zahlerWert;
    private int schrittgroesse;

    public Model(int initialerZaehlerWert, int initialeSchrittgroesse) {
        this.zahlerWert = initialerZaehlerWert;
        this.schrittgroesse = initialeSchrittgroesse;
    }

    public int getZahlerWert() {
        return zahlerWert;
    }

    public void setZahlerWert(int zahlerWert) {
        this.zahlerWert = zahlerWert;
    }

    public int getSchrittgroesse() {
        return schrittgroesse;
    }

    public void setSchrittgroesse(int schrittgroesse) {
        this.schrittgroesse = schrittgroesse;
    }
}
