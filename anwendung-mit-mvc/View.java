import javax.swing.*;
import java.awt.event.ActionListener;

public class View {

    private final JFrame fenster;

    private final JButton buttonErhoehen;
    private final JButton buttonVeringern;

    @SuppressWarnings("FieldCanBeLocal")
    private final JPanel panelSchrittgroesse;
    private final JRadioButton radioButtonEinerSchritte;
    private final JRadioButton radioButtonZehnerSchritte;

    @SuppressWarnings("FieldCanBeLocal")
    private final ButtonGroup radioButtonGroupSchrittgroesse;
    private final JLabel labelZaehlerWert;

    public View(int initialerZaehlerWert, int initialeSchrittgroesse) {
        if (initialeSchrittgroesse != 1 && initialeSchrittgroesse != 10) {
            throw new IllegalArgumentException("Ungültige Schrittgröße! Erlaubt: 1 und 10");
        }

        this.fenster = new JFrame();
        this.fenster.setSize(400, 400);
        this.fenster.setLayout(null);

        this.buttonErhoehen = new JButton("Erhöhen");
        this.buttonErhoehen.setBounds(100, 100, 200, 50);
        this.fenster.add(buttonErhoehen);

        this.buttonVeringern = new JButton("Veringern");
        this.buttonVeringern.setBounds(100, 170, 200, 50);
        this.fenster.add(buttonVeringern);

        this.panelSchrittgroesse = new JPanel();
        this.panelSchrittgroesse.setBounds(100, 230, 200, 80);
        this.fenster.add(panelSchrittgroesse);

        this.radioButtonEinerSchritte = new JRadioButton("Einerschritte");
        this.panelSchrittgroesse.add(radioButtonEinerSchritte);

        this.radioButtonZehnerSchritte = new JRadioButton("Zehnerschritte");
        this.panelSchrittgroesse.add(radioButtonZehnerSchritte);

        this.radioButtonGroupSchrittgroesse = new ButtonGroup();
        this.radioButtonGroupSchrittgroesse.add(radioButtonEinerSchritte);
        this.radioButtonGroupSchrittgroesse.add(radioButtonZehnerSchritte);

        if (initialeSchrittgroesse == 1) {
            this.radioButtonEinerSchritte.setSelected(true);
        } else if (initialeSchrittgroesse == 10) {
            this.radioButtonZehnerSchritte.setSelected(true);
        }

        this.labelZaehlerWert = new JLabel(String.valueOf(initialerZaehlerWert));
        this.labelZaehlerWert.setHorizontalAlignment(SwingConstants.CENTER);
        this.labelZaehlerWert.setBounds(150, 50, 100, 50);
        this.fenster.add(labelZaehlerWert);
    }

    public void setzeZaehlerWert(int zaehlerWert) {
        this.labelZaehlerWert.setText(String.valueOf(zaehlerWert));
    }

    public int gibAusgewaehlteSchrittgroesse() {
        if (radioButtonEinerSchritte.isSelected()) {
            return 1;
        }
        if (radioButtonZehnerSchritte.isSelected()) {
            return 10;
        }

        throw new IllegalStateException("Keine gültige Schrittgröße ausgewählt!");
    }

    public void addButtonErhoehenActionListener(ActionListener actionListener) {
        this.buttonErhoehen.addActionListener(actionListener);
    }

    public void addButtonVeringernActionListener(ActionListener actionListener) {
        this.buttonVeringern.addActionListener(actionListener);
    }

    public void addRadioButtonEinerSchritteActionListener(ActionListener actionListener) {
        this.radioButtonEinerSchritte.addActionListener(actionListener);
    }

    public void addRadioButtonZehnerSchritteActionListener(ActionListener actionListener) {
        this.radioButtonZehnerSchritte.addActionListener(actionListener);
    }

    public void show() {
        this.fenster.setVisible(true);
    }
}
