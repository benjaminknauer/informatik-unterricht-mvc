public class ZaehlerAnwendung {

    public static void main(String[] args) {

        Model meinModel = new Model(0, 1);
        View meineView = new View(meinModel.getZahlerWert(), meinModel.getSchrittgroesse());
        Controller meinController = new Controller(meinModel, meineView);
        // ControllerActionListener meinControllerActionListener = new ControllerActionListener(meinModel, meineView);

    }

}
