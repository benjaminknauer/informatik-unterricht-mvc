import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerActionListener {

    private final Model model;
    private final View view;

    public ControllerActionListener(Model model, View view) {
        this.model = model;
        this.view = view;

        this.view.addButtonErhoehenActionListener(erhoeheZaehlerActionListener);
        this.view.addButtonVeringernActionListener(veringereZaehlerActionListener);
        this.view.addRadioButtonEinerSchritteActionListener(aktualisiereSchrittgroesseActionListener);
        this.view.addRadioButtonZehnerSchritteActionListener(aktualisiereSchrittgroesseActionListener);

        this.view.show();
    }

    ActionListener erhoeheZaehlerActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.setZahlerWert(model.getZahlerWert() + model.getSchrittgroesse());
            view.setzeZaehlerWert(model.getZahlerWert());
        }
    };


    ActionListener veringereZaehlerActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.setZahlerWert(model.getZahlerWert() + model.getSchrittgroesse());
            view.setzeZaehlerWert(model.getZahlerWert());
        }
    };

    ActionListener aktualisiereSchrittgroesseActionListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            model.setSchrittgroesse(view.gibAusgewaehlteSchrittgroesse());
        }
    };

}
